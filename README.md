# K8s Agent on GitLab.com

Playground for the GitLab Kubernetes Agent: https://about.gitlab.com/blog/2021/02/22/gitlab-kubernetes-agent-on-gitlab-com/

[[_TOC_]]

## Useful links

- https://docs.gitlab.com/ee/user/clusters/agent/#create-an-agent-record-in-gitlab
- https://docs.gitlab.com/ee/user/clusters/agent/repository.html
- https://gitlab.com/gitlab-org/configure/examples/gitops-project
- https://gitlab.com/gitlab-examples/ops/configure-product-walkthrough
- https://gitlab.com/-/graphql-explorer

### Blog Posts

- https://everyonecancontribute.com/post/2021-04-28-cafe-27-gitlab-kubernetes-agent-gitops/
- https://everyonecancontribute.com/post/2021-05-05-cafe-28-kubecon-gitlab-kubernetes-agent/
- https://everyonecancontribute.com/post/2021-05-19-cafe-30-kubernetes-monitoring-prometheus/ 

## GitLab Runner

https://docs.gitlab.com/runner/install/kubernetes-agent.html

## Automation

### Terraform

```bash
# Adjust your credentials and source the .envrc file
source .envrc
# run Terraform inot & output Terrafrom plan
terraform init && terraform plan
# run terrafrom apply 
terraform apply -auto-approve
# run terrafrom destroxy
terrafrom destroy 
```

## manual Installation

### GraphQL examples

```json
mutation createAgent {
  createClusterAgent(input: { projectPath: "everyonecancontribute/kubernetes/k8s-agent", name: "ecc" }) {
      clusterAgent {
        id
        name
      }
      errors
    }
  }

# Delete
mutation deleteAgent {
  clusterAgentDelete(input: { id: "<ClusterAgentID>" } ) {
    errors
  }
}

```


```json
mutation createToken {
  clusterAgentTokenCreate(
      input: {
        clusterAgentId: "<ClusterAgentID>"
        description: "everyonecancontribute"
        name: "ecc"
      }
    ) {
      secret
      token {
        createdAt
        id
      }
      errors
    }
  }

# Delete
mutation deleteToken {
  clusterAgentTokenDelete(input: { id: "<ClusterAgentTokenID>" }) {
    errors
  }
}
```

### Kubernetes Agent

```bash
# Create the namespace
kubectl create namespace gitlab-kubernetes-agent
# Create secret with the ClusterAgentTokenSecret
kubectl create secret generic -n gitlab-kubernetes-agent gitlab-kubernetes-agent-token --from-literal=token='<TOKEN>'
# apply the manifest
kubectl apply -n gitlab-kubernetes-agent -f manifest.yaml
# delete the GitLab-Kubernetes-Agent from your Cluster
kubectl -n gitlab-kubernetes-agent delete all --all
kubectl delete ns gitlab-kubernetes-agent
```
