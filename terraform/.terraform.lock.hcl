# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gavinbunney/kubectl" {
  version     = "1.10.0"
  constraints = "~> 1.0"
  hashes = [
    "h1:Sh9IZV2EiEJP240yjK4hn6EZt7aSxcKSonsHEFGGbdg=",
    "zh:0786e6cb375e4e6a70220bb67fc3de80c8c30dcb00c0f4f0ec7bb10404a120db",
    "zh:577347a8334c8cd13215608780e03b77615d211fac64ad6e4356b7f4bb160022",
    "zh:7d3347690a0b68dca54ae5cc90877cf82069f7ef13517668b17fd37f49c91e8c",
    "zh:7f4eeae41b22de803ea7bf8977226c2bc0baaf204a4a2a05c421d9358c907808",
    "zh:8db7a6550374918109d6f445c6c196f02ea3fa2029b882eca186d6e13bd1e4ce",
    "zh:9c93ad71c3039463cf4345acb781c68d7ce82fe8f8495a94a6b588bf87259e51",
    "zh:ee94ff2448caee374f3a3e888568d7ff48e6b9438df76f6eb72efa1aadc6391b",
  ]
}

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.6.0"
  constraints = "~> 3.6.0"
  hashes = [
    "h1:icNPivGM5+Rilvii04ygDEycpSGRzQ4oH+4oXKfhd8M=",
    "zh:08a915f21564737090eea015f47d7dd5c2d1a5f63b49211937534f643f47b8ca",
    "zh:145527bed51ec8070a85e967395fcd9aa32c97fbf2cf42747397cef6f1388213",
    "zh:1e3c8c26af5076b9067ac9f6f71b188c25838db42e8d6c11128322d6c4655982",
    "zh:5a795890e78121f7b8d8a3ae6fdc6a6f69083465b6a2073c9c0f0564dad4f7ec",
    "zh:765219c271e8af16fd436cf88e32079379b035d92bca0df556dee9adf9c9152f",
    "zh:7804d99ba61f74db1a4a911d181e7f8cee62c41658fe424cfa28362d6bbe8784",
    "zh:85e51fd6402de3e842e75ea02dc79cc3903fd030bd3d312bfcb78a8a61b8dfaa",
    "zh:8b13a75dcd932dc43616d058eeae60fada70bc93b0deb7152a4aba55d365e5b0",
    "zh:8d80014cc5acf9c4c64cd73e8f15b418b52cc54e112b1ebb49fdefa2ad8e13eb",
    "zh:b25129758f8aa6835dd22101f945abed6430d457659d20d2638eff27ca71bddd",
    "zh:b35e089343a6185db0196273517fcfe98fbd13d6ac1901b49d1d24b2b39b2ff1",
    "zh:cd928bc0e5d42fb966fd583cb8c46169cf80d35a57859db949d0c0b802fef35d",
    "zh:f8af5be58d41afcda522926e3768728fe412a87dbacca7ecb02341b854bf798f",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.1.0"
  constraints = "~> 2.0"
  hashes = [
    "h1:L/3XfqLQ4bS1PjH/FksJPm+MYIOxCwn97ozbfSwg/VQ=",
    "zh:22e2bcef08fb7f97ed503a27e3725d9d14fdd09fe3aa144fae8a7f78ed27856a",
    "zh:2380cc2a91239b80ea380af8a7fcdcc7396f5213a71a251a5505c962ac6cb9c2",
    "zh:496ea2818d5480590ada763672be051f4e76dc12c6a61fde2faa0c909e174eb7",
    "zh:4e5b6c230d9a8da8a0f12e5db198f158f2c26432ad8e1c6ac22770ce7ec39118",
    "zh:55ad614beffda4cdc918ad87dca09bb7b961f12183c0923230301f73e23e9665",
    "zh:6849c52899091fa2f6714d8e5180a4affffc4b2ad03dc2250043d4b32049e16e",
    "zh:7a6f0d9da5172b3770af98d59263e142313a8b2c4048271893c6003493ad1c89",
    "zh:7c97fb24e60c41fa16f6305620d18ae51545c329f46f92988493a4c51a4e43e5",
    "zh:a08111c4898544c40c62437cc28798d1f4d7298f61ddaf3f48dddec042d3519f",
    "zh:be7493bff6b9f95fe203c295bfc5933111e7c8a5f3bd9e9ae143a0d699d516f8",
    "zh:e4c94adc65b5ad5551893f58c19e1c766f212f16220087ca3e940a89449ac285",
  ]
}

provider "registry.terraform.io/hashicorp/local" {
  version = "2.1.0"
  hashes = [
    "h1:KfieWtVyGWwplSoLIB5usKAUnrIkDQBkWaR5TI+4WYg=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:0wlehNaxBX7GJQnPfQwTNvvAf38Jm0Nv7ssKGMaG6Og=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}

provider "registry.terraform.io/hashicorp/time" {
  version = "0.7.1"
  hashes = [
    "h1:4Ltywoo5WH0igk1rhr6iKz0fGRXg2iMz5+VXV2oZg2E=",
    "zh:1fa9e5b198db3ae7dcfe70ec505db4c70bf3a557e2dcb6fb19cecbe994fe048f",
    "zh:320d90646638ee4d3fa69ee55bce4d9762ac4ceb0530fe114d1ad183c16ea3fb",
    "zh:6a0150852967abf11975f4cb7a7c4190deb871dcf2cdd7697b4955b789e3de63",
    "zh:6f350e7abf170c97d946ae31fcb8b797eca56f8c46f4052a2ba656931a2ca178",
    "zh:7a24ac6a5e53edeb6d50dd573c02f1b36b96c8a9285f83003481cf537f4d61ea",
    "zh:96c3da650bda44b31ba5513e322fd1902d3cfa9cc99129ede70929c71ca74364",
    "zh:98bd13cf906aa58a4e66493f469eed237569a33ba3016e33784e1d8cbdcd0bd6",
    "zh:98c0053a9bef780653a7234046f30ee0ee628e03db5eeeca2646d155422852bc",
    "zh:a009a597a8b4f14da3a76d7fc52c193535c684d12c7f571ff747ea6c1cf52b9d",
    "zh:b7544277dffba3f2eed74a6440564350f7402a3a24329ea8a3bd6c1fad167683",
    "zh:f1bd5f300260e550086266049f85f4912138d6e89bfe01e1e0ba154696e410b2",
  ]
}

provider "registry.terraform.io/hetznercloud/hcloud" {
  version     = "1.26.0"
  constraints = "1.26.0"
  hashes = [
    "h1:pdPUtihsFEadqGcMAaCI7gFgNscyEbu8LzzRB/QgGRk=",
    "zh:03d7eb722a4ee25774949baace0125392060d0369d4cb9257d7d298ab6ece3ff",
    "zh:0fed2e63ac4cb6fe6b2a5b6891abf973cb7c1716e487fbabc09216e0ec05e866",
    "zh:1a84c8c1c8e2d6607de5aa09aa3f9254183cde75a5acc666cca5f4b02a1d290e",
    "zh:23ac426aa3a0001fb20045dc35569978864f139732f45ab671c64e80123c91a1",
    "zh:23b78348b24ae3e4679bd90989c999346efd71ee228d17368d5f556f63e5fd06",
    "zh:2503fe28ac87661af96e7755a7404307000822104ac1abc571271eee46c95ab5",
    "zh:3fe859b2611d20ed5cd65cc2ec812acf73c7dfb39f2fee45ef99a3896c2662a8",
    "zh:51ef869ed35d0d8aada35f587c4a64802f1140dc93c40a4e7c9800560143bb1a",
    "zh:69b93cf4adca465b89da08e4e3b4aaf831821f1fbae68e526c0a292b3cfa463d",
    "zh:6a4e23c6aa86e3d30240e6e4c97daef3af9ad217be2c6f35300fe1839fdbf8b2",
    "zh:97a513459692a981a62b4a566c1d736c4a67622d2fbbee3771ec3ea8d576d484",
    "zh:fec6c07731e23d1dd45015b44747b89c4fee58b5b2560f96d24c7da5a8ecb2ad",
  ]
}

provider "registry.terraform.io/rancher/rke" {
  version     = "1.2.1"
  constraints = "1.2.1"
  hashes = [
    "h1:kPe8pDvdVlW6OGKGWMtMS8ddl6zdimyFtR69r+2dUa8=",
    "zh:49a06fdcbec632665175716defe37e7d9867e539c34cb27e0f36fc3b269f2252",
    "zh:afb4341cf562fccee62509cf61b2eb8889c6666516d745371c1903b672c366ae",
    "zh:c3d7e0bd1a2afb129f293ad9b78b727ff188404445c50abe734f105f2e7d62e0",
    "zh:d9a0bd876a575c21115a58c177f601eb1cde4c11af0f9be8f7d6f024cc89bfae",
    "zh:e1f8bd3a740322eb0e669b08543760db25d5754a703f5773ece50d533d7d57f2",
    "zh:e5f1f4a7d33da5834024fab3c4949e7852afcb99ffa62d5702017309ca661ea7",
  ]
}

provider "registry.terraform.io/sullivtr/graphql" {
  version = "1.4.6"
  hashes = [
    "h1:u4m/uBaHave0VJWMJXaKI1G4lfDJZuC7VIO5+OzEFCY=",
    "zh:1f3dca3a6d957ef90b07e4c4eec169387fc8124f2eb17335290f315cb9638cf2",
    "zh:43b91bdbe888d04fe14ebd7aadb58b201158e0045f154972587bf93fab9e0112",
    "zh:5c93fc774e1a071e151038e39727f449db31eb49d49c50f9df0783bf1e6de295",
    "zh:5debd5ac1b149b128c272eaa82fd2faea8eca68a0ca8418c8f51b13f3000daa5",
    "zh:645df8ffd7ae88f40e70f0e7d82c50ec89d7b9fe754b0cb04a3bdd700d520be3",
    "zh:815eb2d2fd61176d0de2fb2b37f7228da72ea9fe5542d634256babc975b59103",
    "zh:8445d0e6191316bbb58ecd37807a540703bfbf724d97888a786a0046529aa148",
    "zh:8514b68a774403728c76ce43732d1a46cc44541789c8999576d0f1c10cbc8a40",
    "zh:b9f0c62fd26b379d3bbbf3f80a66fbccd7deb04e8eebd06ea570ce02b69a75db",
    "zh:d971abfb5d3c3c500eab2fdd5fdb5ce8d69177b2f97535abdb1f3b6b9788b1f7",
    "zh:e684924ee1c3975f49bbbcec54d40b207890475df100781d8d2ad3eed1f997f2",
    "zh:ea152ca7f1e80ca9a99e3bad543161751a6b062684f602e2a8fff6e62f4e1245",
  ]
}
