data "terraform_remote_state" "ecc" {
  backend = "http"
  config = {
    address  = "${var.remote_address_base}/ecc"
    username = var.gitlab_username
    password = var.gitlab_password
  }
}

data "gitlab_project" "current_project" {
  id = var.gitlab_project_id
}
