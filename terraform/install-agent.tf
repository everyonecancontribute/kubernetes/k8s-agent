locals {
    custom_tags = {
    }
}

provider "kubernetes" {
  host = module.rke.cluster.api_server_url

  client_certificate     = module.rke.cluster.client_cert
  client_key             = module.rke.cluster.client_key
  cluster_ca_certificate = module.rke.cluster.ca_crt
}

resource "kubernetes_namespace" "gitlab-kubernets-agent" {
  metadata {
    annotations = merge({
      name = "gitlab-kubernets-agent"
      }, local.custom_tags
    )
    name = var.agent_namespace
  }
}

resource "kubernetes_secret" "gitlab-kubernets-agent-token" {
  metadata {
    name        = "gitlab-kubernets-agent-token"
    namespace   = kubernetes_namespace.gitlab-kubernets-agent.metadata[0].name
    annotations = local.custom_tags
  }

  data = {
    token = graphql_mutation.agent_token.computed_read_operation_variables.secret
  }
}

resource "kubernetes_service_account" "gitlab-kubernets-agent" {
  metadata {
    name        = "gitlab-kubernets-agent"
    namespace   = kubernetes_namespace.gitlab-kubernets-agent.metadata[0].name
    annotations = local.custom_tags
  }
  // secret {
  //   name = kubernetes_secret.gitlab-kubernets-agent-token.metadata.0.name
  // }
}

resource "kubernetes_cluster_role" "cilium-alert-read" {
  metadata {
    name        = "cilium-alert-read"
    annotations = local.custom_tags
  }

  rule {
    api_groups = ["cilium.io"]
    resources  = ["ciliumnetworkpolicies"]
    verbs      = ["list", "watch"]
  }
}

resource "kubernetes_cluster_role" "gitlab-kubernets-agent-gitops-read-all" {
  metadata {
    name        = "gitlab-kubernets-agent-gitops-read-all"
    annotations = local.custom_tags
  }

  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs      = ["get", "list", "watch"]
  }
}

resource "kubernetes_cluster_role" "gitlab-kubernets-agent-gitops-write-all" {
  metadata {
    name        = "gitlab-kubernets-agent-gitops-write-all"
    annotations = local.custom_tags
  }

  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs      = ["create", "update", "delete", "patch"]
  }
}

resource "kubernetes_cluster_role_binding" "cilium-alert-read" {
  metadata {
    name = "cilium-alert-read"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.cilium-alert-read.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.gitlab-kubernets-agent.metadata[0].name
    namespace = kubernetes_namespace.gitlab-kubernets-agent.metadata[0].name
  }
}

resource "kubernetes_cluster_role_binding" "gitlab-kubernets-agent-gitops-read-all" {
  metadata {
    name = "gitlab-kubernets-agent-gitops-read-all"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.gitlab-kubernets-agent-gitops-read-all.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.gitlab-kubernets-agent.metadata[0].name
    namespace = kubernetes_namespace.gitlab-kubernets-agent.metadata[0].name
  }
}

resource "kubernetes_cluster_role_binding" "gitlab-kubernets-agent-gitops-write-all" {
  metadata {
    name = "gitlab-kubernets-agent-gitops-write-all"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.gitlab-kubernets-agent-gitops-write-all.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.gitlab-kubernets-agent.metadata[0].name
    namespace = kubernetes_namespace.gitlab-kubernets-agent.metadata[0].name
  }
}

resource "kubernetes_deployment" "gitlab-kubernets-agent" {
  wait_for_rollout = false
  metadata {
    name        = "gitlab-kubernets-agent"
    namespace   = kubernetes_namespace.gitlab-kubernets-agent.metadata[0].name
    annotations = local.custom_tags
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "gitlab-kubernets-agent"
      }
    }

    template {
      metadata {
        labels = {
          app = "gitlab-kubernets-agent"
        }
        annotations = local.custom_tags
      }

      spec {
        service_account_name = kubernetes_service_account.gitlab-kubernets-agent.metadata[0].name
        container {
          image = "registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/agentk:${var.agent_version}"
          name  = "agent"
          args = [
            "--token-file=/config/token",
            "--kas-address",
            "wss://kas.gitlab.com",
          ]
          volume_mount {
            name       = "token-volume"
            mount_path = "/config"
          }
        }

        volume {
          name = "token-volume"
          secret {
            secret_name = kubernetes_secret.gitlab-kubernets-agent-token.metadata.0.name
          }
        }
      }
    }
    strategy {
      type = "RollingUpdate"
      rolling_update {
        max_surge       = 0
        max_unavailable = 1
      }
    }
  }
}
