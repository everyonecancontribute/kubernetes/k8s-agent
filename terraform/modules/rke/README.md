## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_rke"></a> [rke](#requirement\_rke) | 1.2.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_rke"></a> [rke](#provider\_rke) | 1.2.1 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [rke_cluster.rancher-cluster](https://registry.terraform.io/providers/rancher/rke/1.2.1/docs/resources/cluster) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_environment_name"></a> [environment\_name](#input\_environment\_name) | n/a | `any` | n/a | yes |
| <a name="input_kubernetes_version"></a> [kubernetes\_version](#input\_kubernetes\_version) | n/a | `string` | `"v1.20.4-rancher1-1"` | no |
| <a name="input_nodes"></a> [nodes](#input\_nodes) | n/a | `any` | n/a | yes |
| <a name="input_private_ssh_key_file"></a> [private\_ssh\_key\_file](#input\_private\_ssh\_key\_file) | n/a | `string` | `"~/.ssh/id_ed25519.pub"` | no |
| <a name="input_ssh_user"></a> [ssh\_user](#input\_ssh\_user) | n/a | `string` | `"root"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cluster"></a> [cluster](#output\_cluster) | n/a |
