variable "nodes" {

}

variable "ssh_user" {
  default = "root"
}
variable "private_ssh_key_file" {
  default = "~/.ssh/id_ed25519.pub"
}
variable "environment_name" {

}
variable "kubernetes_version" {
  default = "v1.20.4-rancher1-1"
}
