resource "hcloud_ssh_key" "user" {
  name       = "user"
  public_key = file("~/.ssh/id_ed25519.pub")
}
