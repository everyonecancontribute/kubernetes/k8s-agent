output "ssh_id" {
  description = "The ID of the SSH key"
  value       = hcloud_ssh_key.user.id
}
