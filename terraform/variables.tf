#########################################################
################## Hetzner CLoud & K1  ##################
#########################################################
variable "hcloud_token" {
  type = string
}

#########################################################
################## RKE                 ##################
#########################################################

variable "environment_name" {
  default = "rke-test"
}

variable "nodes" {
  description = "Map of project names to configuration."
  type        = map(any)
  default = {
    server01 = {
      server_type = "cx21",
      label       = "controlplane"
    },
    node01 = {
      server_type = "cpx11",
      label       = "node"
    }
    node02 = {
      server_type = "cpx11",
      label       = "node"
    }
  }
}

#########################################################
################## GitLab              ##################
#########################################################
variable "remote_address_base" {
  type = string
}

variable "gitlab_project_id" {
  type = string
}

variable "gitlab_username" {
  type = string
}

variable "gitlab_password" {
  type = string
}

variable "agent_name" {
  type    = string
  default = "ecc"
}

variable "token_name" {
  type    = string
  default = "kas-token"
}

variable "token_description" {
  type    = string
  default = "Token for KAS Agent Authentication"
}

variable "gitlab_graphql_api_url" {
  type    = string
  default = "https://gitlab.com/api/graphql"
}

variable "agent_namespace" {
  type    = string
  default = "gitlab-kubernetes-agent"
}

variable "agent_version" {
  type    = string
  default = "stable"
}
