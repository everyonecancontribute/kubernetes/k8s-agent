terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.26.0"
    }
    kubernetes = "~> 2.0"
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "~> 1.0"
    }
    graphql = {
      source = "sullivtr/graphql"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~>3.6.0"
    }
    rke = {
      source  = "rancher/rke"
      version = "1.2.1"
    }
  }
  required_version = ">= 0.13"
}
